﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class mainmenu : Form
    {
        public mainmenu()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.Focus();
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //button play
            this.playbutton.BackgroundImage = new Bitmap("buttonstartgame.png");
            this.playbutton.BackgroundImageLayout = ImageLayout.Stretch;
            this.playbutton.BackColor = Color.Transparent;
            this.playbutton.ForeColor = Color.Transparent;
            this.playbutton.FlatStyle = FlatStyle.Flat;
            //this.playbutton.Location = new Point(560, 304);
            //button load
            this.loadbutton.BackgroundImage = new Bitmap("loadgame.png");
            this.loadbutton.BackgroundImageLayout = ImageLayout.Stretch;
            this.loadbutton.BackColor = Color.Transparent;
            this.loadbutton.ForeColor = Color.Transparent;
            this.loadbutton.FlatStyle = FlatStyle.Flat;
             //this.playbutton.Location = new Point(560, 387);
            //button exit
            this.exitbutton.BackgroundImage = new Bitmap("exitgame.png");
            this.exitbutton.BackgroundImageLayout = ImageLayout.Stretch;
            this.exitbutton.BackColor = Color.Transparent;
            this.exitbutton.ForeColor = Color.Transparent;
            this.exitbutton.FlatStyle = FlatStyle.Flat;
            //this.playbutton.Location = new Point(560, 468);
            //label title
            this.title.BackgroundImage = new Bitmap("TITLE.png");
            this.title.BackgroundImageLayout = ImageLayout.Stretch;
            this.title.BackColor = Color.Black;
            this.title.FlatStyle =FlatStyle.Flat;
        }

        private void exitbutton_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).exit_game();
        }

        private void playbutton_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_select_player();
            ((Form1)MdiParent).save_or_load = 0;
        }

        private void loadbutton_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_select_player_load();
            ((Form1)MdiParent).save_or_load = 1;
        }

        private void playbutton_MouseHover(object sender, EventArgs e)
        {
            this.playbutton.BackColor = Color.BlueViolet;
        }

        private void playbutton_MouseLeave(object sender, EventArgs e)
        {
            this.playbutton.BackColor = Color.Transparent;
        }

        private void loadbutton_MouseHover(object sender, EventArgs e)
        {
            this.loadbutton.BackColor = Color.BlueViolet;
        }

        private void loadbutton_MouseLeave(object sender, EventArgs e)
        {
            this.loadbutton.BackColor = Color.Transparent;
        }

        private void exitbutton_MouseHover(object sender, EventArgs e)
        {
            this.exitbutton.BackColor = Color.BlueViolet;
        }

        private void exitbutton_MouseLeave(object sender, EventArgs e)
        {
            this.exitbutton.BackColor = Color.Transparent;
        }
    }
}
       