﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class loadingscreen : Form
    {
        int ctr = 0;
        public loadingscreen()
        {
            InitializeComponent();
        }

        private void loadingscreen_Load(object sender, EventArgs e)
        {
            //setting form
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.BackgroundImage = new Bitmap("backgroundloadingsc1.png");
            // this.BackgroundImageLayout = ImageLayout.Stretch;
            //this.BackColor = Color.Transparent;

            //label title
            this.label1.BackgroundImage = new Bitmap("TITLE.png");
            this.label1.BackgroundImageLayout = ImageLayout.Stretch;
            this.label1.BackColor = Color.Transparent;
            this.label1.FlatStyle = FlatStyle.Flat;
            //button press
            this.pressbutton.BackgroundImage = new Bitmap("buttonpress.png");
            this.pressbutton.BackgroundImageLayout = ImageLayout.Stretch;
            this.pressbutton.BackColor = Color.Transparent;
            this.pressbutton.FlatStyle = FlatStyle.Flat;
            //setting press this button
            this.pressbutton.Visible = false;
            
            //loading.Width = this.Width;
            //loading.BackgroundImageLayout = ImageLayout.Stretch;
            
            loading_progress.Start();
        }

        private void pressbutton_Click(object sender, EventArgs e)
        {
            ((Form1)MdiParent).change_to_mainmenu();
        }
        private void loading_progress_Tick(object sender, EventArgs e)
        {
            ctr++;
            if (ctr==51)
            {
                this.pressbutton.Visible = true;
                loading.Visible = false;
                loading_progress.Stop();
            }
        }

        private void pressbutton_MouseHover(object sender, EventArgs e)
        {
            this.pressbutton.BackColor = Color.BlueViolet;
        }

        private void pressbutton_MouseLeave(object sender, EventArgs e)
        {
            this.pressbutton.BackColor = Color.Transparent;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
