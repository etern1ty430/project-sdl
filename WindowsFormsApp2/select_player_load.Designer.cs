﻿namespace WindowsFormsApp2
{
    partial class select_player_load
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.Button();
            this.display_player = new System.Windows.Forms.Button();
            this.create_player = new System.Windows.Forms.Button();
            this.player4 = new System.Windows.Forms.Button();
            this.player3 = new System.Windows.Forms.Button();
            this.player2 = new System.Windows.Forms.Button();
            this.player1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(340, 565);
            this.back.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(211, 57);
            this.back.TabIndex = 19;
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // display_player
            // 
            this.display_player.Location = new System.Drawing.Point(700, 86);
            this.display_player.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.display_player.Name = "display_player";
            this.display_player.Size = new System.Drawing.Size(338, 488);
            this.display_player.TabIndex = 18;
            this.display_player.UseVisualStyleBackColor = true;
            // 
            // create_player
            // 
            this.create_player.Location = new System.Drawing.Point(340, 336);
            this.create_player.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.create_player.Name = "create_player";
            this.create_player.Size = new System.Drawing.Size(211, 57);
            this.create_player.TabIndex = 17;
            this.create_player.UseVisualStyleBackColor = true;
            this.create_player.Click += new System.EventHandler(this.create_player_Click);
            // 
            // player4
            // 
            this.player4.Location = new System.Drawing.Point(550, 134);
            this.player4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.player4.Name = "player4";
            this.player4.Size = new System.Drawing.Size(56, 57);
            this.player4.TabIndex = 16;
            this.player4.UseVisualStyleBackColor = true;
            this.player4.Click += new System.EventHandler(this.player4_Click);
            // 
            // player3
            // 
            this.player3.Location = new System.Drawing.Point(450, 134);
            this.player3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.player3.Name = "player3";
            this.player3.Size = new System.Drawing.Size(56, 57);
            this.player3.TabIndex = 15;
            this.player3.UseVisualStyleBackColor = true;
            this.player3.Click += new System.EventHandler(this.player3_Click);
            // 
            // player2
            // 
            this.player2.Location = new System.Drawing.Point(350, 134);
            this.player2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.player2.Name = "player2";
            this.player2.Size = new System.Drawing.Size(56, 57);
            this.player2.TabIndex = 14;
            this.player2.UseVisualStyleBackColor = true;
            this.player2.Click += new System.EventHandler(this.player2_Click);
            // 
            // player1
            // 
            this.player1.Location = new System.Drawing.Point(250, 134);
            this.player1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(56, 57);
            this.player1.TabIndex = 13;
            this.player1.UseVisualStyleBackColor = true;
            this.player1.Click += new System.EventHandler(this.player1_Click);
            // 
            // select_player_load
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 577);
            this.Controls.Add(this.back);
            this.Controls.Add(this.display_player);
            this.Controls.Add(this.create_player);
            this.Controls.Add(this.player4);
            this.Controls.Add(this.player3);
            this.Controls.Add(this.player2);
            this.Controls.Add(this.player1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "select_player_load";
            this.Text = "select_player_load";
            this.Load += new System.EventHandler(this.select_player_load_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button display_player;
        private System.Windows.Forms.Button create_player;
        private System.Windows.Forms.Button player4;
        private System.Windows.Forms.Button player3;
        private System.Windows.Forms.Button player2;
        private System.Windows.Forms.Button player1;
    }
}