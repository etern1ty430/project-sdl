﻿namespace WindowsFormsApp2
{
    partial class shop_game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dg_bomb = new System.Windows.Forms.DataGridView();
            this.dg_glove = new System.Windows.Forms.DataGridView();
            this.dg_upgrade = new System.Windows.Forms.DataGridView();
            this.back = new System.Windows.Forms.Button();
            this.pdeskripsi = new System.Windows.Forms.Panel();
            this.pbomb = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_bomplayer = new System.Windows.Forms.Label();
            this.label_skillplayer = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.GroupBox();
            this.labelUang = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_upgrade = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dg_bomb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_glove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_upgrade)).BeginInit();
            this.Status.SuspendLayout();
            this.SuspendLayout();
            // 
            // dg_bomb
            // 
            this.dg_bomb.AllowUserToAddRows = false;
            this.dg_bomb.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_bomb.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dg_bomb.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_bomb.DefaultCellStyle = dataGridViewCellStyle1;
            this.dg_bomb.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dg_bomb.Location = new System.Drawing.Point(16, 43);
            this.dg_bomb.Margin = new System.Windows.Forms.Padding(4);
            this.dg_bomb.Name = "dg_bomb";
            this.dg_bomb.Size = new System.Drawing.Size(703, 128);
            this.dg_bomb.TabIndex = 0;
            this.dg_bomb.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_bomb_CellContentClick);
            // 
            // dg_glove
            // 
            this.dg_glove.AllowUserToAddRows = false;
            this.dg_glove.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_glove.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dg_glove.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_glove.DefaultCellStyle = dataGridViewCellStyle2;
            this.dg_glove.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dg_glove.Location = new System.Drawing.Point(16, 211);
            this.dg_glove.Margin = new System.Windows.Forms.Padding(4);
            this.dg_glove.Name = "dg_glove";
            this.dg_glove.Size = new System.Drawing.Size(703, 98);
            this.dg_glove.TabIndex = 1;
            this.dg_glove.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_glove_CellClick);
            this.dg_glove.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_glove_CellContentClick);
            // 
            // dg_upgrade
            // 
            this.dg_upgrade.AllowUserToAddRows = false;
            this.dg_upgrade.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_upgrade.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dg_upgrade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_upgrade.DefaultCellStyle = dataGridViewCellStyle3;
            this.dg_upgrade.Location = new System.Drawing.Point(16, 354);
            this.dg_upgrade.Margin = new System.Windows.Forms.Padding(4);
            this.dg_upgrade.Name = "dg_upgrade";
            this.dg_upgrade.Size = new System.Drawing.Size(703, 170);
            this.dg_upgrade.TabIndex = 2;
            this.dg_upgrade.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_upgrade_CellContentClick);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(1077, 629);
            this.back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(336, 73);
            this.back.TabIndex = 3;
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // pdeskripsi
            // 
            this.pdeskripsi.Location = new System.Drawing.Point(915, 334);
            this.pdeskripsi.Margin = new System.Windows.Forms.Padding(4);
            this.pdeskripsi.Name = "pdeskripsi";
            this.pdeskripsi.Size = new System.Drawing.Size(652, 242);
            this.pdeskripsi.TabIndex = 5;
            // 
            // pbomb
            // 
            this.pbomb.Location = new System.Drawing.Point(1077, 15);
            this.pbomb.Margin = new System.Windows.Forms.Padding(4);
            this.pbomb.Name = "pbomb";
            this.pbomb.Size = new System.Drawing.Size(336, 294);
            this.pbomb.TabIndex = 5;
            this.pbomb.Paint += new System.Windows.Forms.PaintEventHandler(this.pbomb_Paint);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(480, 63);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(215, 295);
            this.panel1.TabIndex = 6;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(144, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(344, 44);
            this.label1.TabIndex = 7;
            this.label1.Text = "STATUS PLAYER";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 89);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 28);
            this.label2.TabIndex = 8;
            this.label2.Text = "BOM PLAYER :";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(168, 88);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 28);
            this.label3.TabIndex = 9;
            this.label3.Text = "ITEM PLAYER :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(168, 181);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 28);
            this.label4.TabIndex = 10;
            this.label4.Text = "UPGRADE :";
            // 
            // label_bomplayer
            // 
            this.label_bomplayer.Location = new System.Drawing.Point(12, 117);
            this.label_bomplayer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_bomplayer.Name = "label_bomplayer";
            this.label_bomplayer.Size = new System.Drawing.Size(129, 182);
            this.label_bomplayer.TabIndex = 11;
            this.label_bomplayer.Text = "isi data Bom Player  yang ada (bom-bomnya)";
            // 
            // label_skillplayer
            // 
            this.label_skillplayer.Location = new System.Drawing.Point(172, 117);
            this.label_skillplayer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_skillplayer.Name = "label_skillplayer";
            this.label_skillplayer.Size = new System.Drawing.Size(455, 39);
            this.label_skillplayer.TabIndex = 12;
            this.label_skillplayer.Text = "isi skill player yang dimiliki player";
            // 
            // Status
            // 
            this.Status.Controls.Add(this.labelUang);
            this.Status.Controls.Add(this.label6);
            this.Status.Controls.Add(this.panel1);
            this.Status.Controls.Add(this.label_upgrade);
            this.Status.Controls.Add(this.label1);
            this.Status.Controls.Add(this.label_skillplayer);
            this.Status.Controls.Add(this.label3);
            this.Status.Controls.Add(this.label_bomplayer);
            this.Status.Controls.Add(this.label2);
            this.Status.Controls.Add(this.label4);
            this.Status.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Status.Location = new System.Drawing.Point(16, 532);
            this.Status.Margin = new System.Windows.Forms.Padding(4);
            this.Status.Name = "Status";
            this.Status.Padding = new System.Windows.Forms.Padding(4);
            this.Status.Size = new System.Drawing.Size(703, 366);
            this.Status.TabIndex = 14;
            this.Status.TabStop = false;
            this.Status.Text = "Status";
            // 
            // labelUang
            // 
            this.labelUang.Location = new System.Drawing.Point(12, 318);
            this.labelUang.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUang.Name = "labelUang";
            this.labelUang.Size = new System.Drawing.Size(455, 39);
            this.labelUang.TabIndex = 15;
            this.labelUang.Text = "isi dengan data yang di upgrade  player";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 290);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 28);
            this.label6.TabIndex = 14;
            this.label6.Text = "UANG :";
            // 
            // label_upgrade
            // 
            this.label_upgrade.Location = new System.Drawing.Point(168, 209);
            this.label_upgrade.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_upgrade.Name = "label_upgrade";
            this.label_upgrade.Size = new System.Drawing.Size(455, 39);
            this.label_upgrade.TabIndex = 13;
            this.label_upgrade.Text = "isi dengan data yang di upgrade  player";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(756, 649);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // shop_game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 912);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.pbomb);
            this.Controls.Add(this.pdeskripsi);
            this.Controls.Add(this.back);
            this.Controls.Add(this.dg_upgrade);
            this.Controls.Add(this.dg_glove);
            this.Controls.Add(this.dg_bomb);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "shop_game";
            this.Text = "shop_game";
            this.Load += new System.EventHandler(this.shop_game_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg_bomb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_glove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_upgrade)).EndInit();
            this.Status.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dg_bomb;
        private System.Windows.Forms.DataGridView dg_glove;
        private System.Windows.Forms.DataGridView dg_upgrade;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Panel pdeskripsi;
        private System.Windows.Forms.Panel pbomb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_bomplayer;
        private System.Windows.Forms.Label label_skillplayer;
        private System.Windows.Forms.GroupBox Status;
        private System.Windows.Forms.Label labelUang;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_upgrade;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
    }
}