﻿namespace WindowsFormsApp2
{
    partial class menugame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playbutton = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.Button();
            this.shop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // playbutton
            // 
            this.playbutton.Location = new System.Drawing.Point(747, 374);
            this.playbutton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.playbutton.Name = "playbutton";
            this.playbutton.Size = new System.Drawing.Size(336, 73);
            this.playbutton.TabIndex = 0;
            this.playbutton.UseVisualStyleBackColor = true;
            this.playbutton.Click += new System.EventHandler(this.playbutton_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(747, 576);
            this.exit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(336, 73);
            this.exit.TabIndex = 2;
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // title
            // 
            this.title.Location = new System.Drawing.Point(501, 37);
            this.title.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(836, 234);
            this.title.TabIndex = 5;
            this.title.UseVisualStyleBackColor = true;
            // 
            // shop
            // 
            this.shop.Location = new System.Drawing.Point(747, 476);
            this.shop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.shop.Name = "shop";
            this.shop.Size = new System.Drawing.Size(336, 73);
            this.shop.TabIndex = 6;
            this.shop.UseVisualStyleBackColor = true;
            this.shop.Click += new System.EventHandler(this.shop_Click);
            // 
            // menugame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 750);
            this.Controls.Add(this.shop);
            this.Controls.Add(this.title);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.playbutton);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "menugame";
            this.Text = "menugame";
            this.Load += new System.EventHandler(this.menugame_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button playbutton;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Button title;
        private System.Windows.Forms.Button shop;
    }
}