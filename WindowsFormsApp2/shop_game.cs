﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace WindowsFormsApp2
{
    public partial class shop_game : Form
    {
        //class unit dan bomb
        public class inventory_bomb
        {
            public int horizontal;
            public int vertical;
            public inventory_bomb() { }
            public inventory_bomb(int horizontal, int vertical)
            {
                this.horizontal = horizontal;
                this.vertical = vertical;
            }
        }
        public class splash_bomb
        {
            public int ctrbomb = 0;
            public bool tanam = false;
            public bool meledak = false;
            public bool ledakan = false;
            public int ctrledak = 0;
            //mine
            public inventory_bomb isi_bomb = new inventory_bomb(1, 1);
            public bool plantinmap = false;
            public Label label_unit;
            public List<Label> label_ledakan = new List<Label>();
            public splash_bomb() { }
            public splash_bomb(Label labelunit)
            {
                this.label_unit = labelunit;
            }
        }
        public class inventory_unit
        {
            public int uang = 100000;
            public int mana_skill = 20;
            public int speed_up = 45;
            public int time_speed_up = 3;
            public int Movement_Speed = 75;
            public int time_planting = 200;
            public int timeEE = 1;
            public int jumlah_bomb = 1;
            public string skill = "";
            public string[] listUpgrade = new string[3];
            public string[] listBomb = new string[3];
            public int ctrlistUpgrade = 0;
            public int ctrlistBomb = 0;
            public string gloveShop = "-";
            public int level_skill = 0;
            public inventory_unit() { }
        }
        public class unit
        {
            public inventory_unit bag = new inventory_unit();
            public splash_bomb mine = new splash_bomb(new Label());
            public int arah;
            public Timer animasi_gerak = new Timer();
            public Timer animasi_planting_bomb = new Timer();
            public Label label_unit;
            public List<splash_bomb> bomb = new List<splash_bomb>();
            public int ctrgerak = 0;
            public int ctrtimer = 0;
            public bool gerak = false;
            public bool use_skill = false;
            public bool planting_bomb = false;
            public ProgressBar mana = new ProgressBar();
            public Image[] atas = new Image[3];
            public Image[] bawah = new Image[3];
            public Image[] kanan = new Image[3];
            public Image[] kiri = new Image[3];
            public Image[] image_bomb = new Image[3];
            public unit() { }
            public unit(Label labelunit, int choose)
            {
                if (choose == 0)
                {
                    this.atas[0] = Image.FromFile("black\\a0.png");
                    this.atas[1] = Image.FromFile("black\\a1.png");
                    this.atas[2] = Image.FromFile("black\\a2.png");
                    this.bawah[0] = Image.FromFile("black\\b0.png");
                    this.bawah[1] = Image.FromFile("black\\b1.png");
                    this.bawah[2] = Image.FromFile("black\\b2.png");
                    this.kiri[0] = Image.FromFile("black\\kiri0.png");
                    this.kiri[1] = Image.FromFile("black\\kiri1.png");
                    this.kiri[2] = Image.FromFile("black\\kiri2.png");
                    this.kanan[0] = Image.FromFile("black\\k0.png");
                    this.kanan[1] = Image.FromFile("black\\k1.png");
                    this.kanan[2] = Image.FromFile("black\\k2.png");
                    this.image_bomb[0] = Image.FromFile("black\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("black\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("black\\bombawah.png");
                }
                else if (choose == 1)
                {
                    this.atas[0] = Image.FromFile("green\\a0.png");
                    this.atas[1] = Image.FromFile("green\\a1.png");
                    this.atas[2] = Image.FromFile("green\\a2.png");
                    this.bawah[0] = Image.FromFile("green\\b0.png");
                    this.bawah[1] = Image.FromFile("green\\b1.png");
                    this.bawah[2] = Image.FromFile("green\\b2.png");
                    this.kiri[0] = Image.FromFile("green\\kiri0.png");
                    this.kiri[1] = Image.FromFile("green\\kiri1.png");
                    this.kiri[2] = Image.FromFile("green\\kiri2.png");
                    this.kanan[0] = Image.FromFile("green\\k0.png");
                    this.kanan[1] = Image.FromFile("green\\k1.png");
                    this.kanan[2] = Image.FromFile("green\\k2.png");
                    this.image_bomb[0] = Image.FromFile("green\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("green\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("green\\bombawah.png");
                }
                else if (choose == 2)
                {
                    this.atas[0] = Image.FromFile("yellow\\a0.png");
                    this.atas[1] = Image.FromFile("yellow\\a1.png");
                    this.atas[2] = Image.FromFile("yellow\\a2.png");
                    this.bawah[0] = Image.FromFile("yellow\\b0.png");
                    this.bawah[1] = Image.FromFile("yellow\\b1.png");
                    this.bawah[2] = Image.FromFile("yellow\\b2.png");
                    this.kiri[0] = Image.FromFile("yellow\\kiri0.png");
                    this.kiri[1] = Image.FromFile("yellow\\kiri1.png");
                    this.kiri[2] = Image.FromFile("yellow\\kiri2.png");
                    this.kanan[0] = Image.FromFile("yellow\\k0.png");
                    this.kanan[1] = Image.FromFile("yellow\\k1.png");
                    this.kanan[2] = Image.FromFile("yellow\\k2.png");
                    this.image_bomb[0] = Image.FromFile("yellow\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("yellow\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("yellow\\bombawah.png");
                }
                else if (choose == 3)
                {
                    this.atas[0] = Image.FromFile("blue\\a0.png");
                    this.atas[1] = Image.FromFile("blue\\a1.png");
                    this.atas[2] = Image.FromFile("blue\\a2.png");
                    this.bawah[0] = Image.FromFile("blue\\b0.png");
                    this.bawah[1] = Image.FromFile("blue\\b1.png");
                    this.bawah[2] = Image.FromFile("blue\\b2.png");
                    this.kiri[0] = Image.FromFile("blue\\kiri0.png");
                    this.kiri[1] = Image.FromFile("blue\\kiri1.png");
                    this.kiri[2] = Image.FromFile("blue\\kiri2.png");
                    this.kanan[0] = Image.FromFile("blue\\k0.png");
                    this.kanan[1] = Image.FromFile("blue\\k1.png");
                    this.kanan[2] = Image.FromFile("blue\\k2.png");
                    this.image_bomb[0] = Image.FromFile("blue\\bombawah.png");
                    this.image_bomb[1] = Image.FromFile("blue\\bomatas.png");
                    this.image_bomb[2] = Image.FromFile("blue\\bombawah.png");
                }
                this.animasi_gerak.Tick += new EventHandler(animasi);
                this.animasi_planting_bomb.Tick += new EventHandler(timer_plant_bomb);
                this.arah = 0;
                this.label_unit = labelunit;
                this.mana.Minimum = 0;
                this.mana.Maximum = 100;
                this.mana.Size = new Size(50, 10);
                this.mana.Value = 100;
                this.mana.Style = ProgressBarStyle.Continuous;
                this.mana.ForeColor = Color.Turquoise;
            }
            public void animasi(object sender, EventArgs e)
            {
                if (ctrgerak < 5 && gerak == true)
                {
                    animasi_gerak.Interval = bag.Movement_Speed;
                    if (arah == 1)
                    {
                        label_unit.Image = atas[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 10);
                        mana.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 10);
                    }
                    else if (arah == 2)
                    {
                        label_unit.Image = bawah[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y + 10);
                        mana.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 10);
                    }
                    else if (arah == 3)
                    {
                        label_unit.Image = kiri[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X - 10, label_unit.Location.Y);
                        mana.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 10);
                    }
                    else if (arah == 4)
                    {
                        label_unit.Image = kanan[ctrgerak % 3];
                        label_unit.Location = new Point(label_unit.Location.X + 10, label_unit.Location.Y);
                        mana.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 10);
                    }
                    ctrgerak++;
                }
                else
                {
                    gerak = false;
                    ctrgerak = 0;
                }
            }
            public void timer_plant_bomb(object sender, EventArgs e)
            {
                if (planting_bomb == true)
                {
                    animasi_planting_bomb.Interval = bag.time_planting;
                    int index = -1;
                    for (int j = 0; j < bomb.Count; j++)
                    {
                        if (bomb[j].ctrbomb < 3 && bomb[j].tanam == true)
                        {
                            label_unit.Image = image_bomb[bomb[j].ctrbomb];
                            if (bomb[j].ctrbomb == 0)
                            {
                                bomb[j].label_unit.Image = new Bitmap("bomb1.png");
                                bomb[j].label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y + 50);
                                bomb[j].label_unit.BringToFront();
                            }
                            else if (bomb[j].ctrbomb == 1)
                            {
                                bomb[j].label_unit.Image = new Bitmap("bomb1.png");
                                bomb[j].label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y - 50);
                            }
                            else
                            {
                                bomb[j].label_unit.Image = new Bitmap("bomb1.png");
                                bomb[j].label_unit.Location = new Point(label_unit.Location.X, label_unit.Location.Y);
                                label_unit.BringToFront();
                            }
                            bomb[j].ctrbomb++;
                        }
                        else if (bomb[j].ctrbomb == 3 && bomb[j].tanam == true)
                        {
                            planting_bomb = false;
                            bomb[j].tanam = false;
                            bomb[j].ctrbomb = 0;
                            bomb[j].meledak = true;
                            gerak = false;
                        }
                    }
                }
            }
        }
        //start program
        DataTable bomb        = new DataTable("bom");
        DataTable dglove      = new DataTable("glove");
        DataTable dgupgrade   = new DataTable("upgrade");
        List<int> bomver      = new List<int>();
        List<int> bomhor      = new List<int>();
        List<int> glove       = new List<int>();
        List<int> upgrade     = new List<int>();
        //player
        public unit[] arr_unit = new unit[4];
        public string[] arr_save = new string[4];
        public string[] arr_mine = new string[4];
        public string[] arr_bomb = new string[4];
        public string[] arr_gambar = new string[4];
        int temp_bom          = 0;
        int totalbomver       = 200;
        int totalbomhor       = 200;
        int totalbomplus      = 200;
        int uangPlayer        = -1;
        public shop_game()
        {
            InitializeComponent();
        }

        public void buatgridbomb()
        {
            bomb = new DataTable("bom");
            bomb.Columns.Add("Nama Bomb", typeof(string));
            bomb.Columns.Add("Harga", typeof(int));

            bomb.ReadXml("databomb.xml");
            dg_bomb.DataSource = bomb;
            dg_bomb.Columns[0].Width = 320;
            dg_bomb.Columns[1].Width = 150;
            
            dg_bomb.Columns[0].HeaderText = "N A M A   B O M B";
            dg_bomb.Columns[1].HeaderText = "H A R G A";
            DataGridViewButtonColumn bcplus = new DataGridViewButtonColumn();
            bcplus.UseColumnTextForButtonValue = true;
            bcplus.Text = "+";
            dg_bomb.Columns.Add(bcplus);
            dg_bomb.Columns[2].HeaderText = "ADD";
        }
        public void buatgridglove()
        {
            dglove = new DataTable("glove");
            dglove.Columns.Add("Nama ", typeof(string));
            dglove.Columns.Add("Harga", typeof(int));
            
            DataRow drow = dglove.NewRow();
            drow[0] = "Speed Glove 1";
            drow[1] = 200;

            dglove.ReadXml("dataglove.xml");
            dg_glove.DataSource = dglove;
            dg_glove.Columns[0].Width = 320;
            dg_bomb.Columns[1].Width = 150;
            dg_glove.Columns[0].HeaderText = "N A M A   G L O V E";
            dg_glove.Columns[1].HeaderText = "H A R G A";
            DataGridViewButtonColumn bcplus = new DataGridViewButtonColumn();
            bcplus.UseColumnTextForButtonValue = true;
            bcplus.Text = "+";
            dg_glove.Columns.Add(bcplus);
            dg_glove.Columns[2].HeaderText = "ADD";
        }

        public void buatgridupgrade()
        {
            dgupgrade = new DataTable("upgrade");
            dgupgrade.Columns.Add("Nama Bomb", typeof(string));
            dgupgrade.Columns.Add("Harga", typeof(int));

            DataRow drow = dgupgrade.NewRow();
            drow[0] = "Bomb Up + 1";
            drow[1] = 150;

            dgupgrade.ReadXml("dataupgrade.xml");
            dg_upgrade.DataSource = dgupgrade;
            dg_upgrade.Columns[0].Width = 500;
            dg_upgrade.Columns[1].Width = 150;
            dg_upgrade.Columns[0].HeaderText = "N A M A   S K I L L";
            dg_upgrade.Columns[1].HeaderText = "H A R G A";
            DataGridViewButtonColumn bcplus = new DataGridViewButtonColumn();
            bcplus.UseColumnTextForButtonValue = true;
            bcplus.Text = "+";
            dg_upgrade.Columns.Add(bcplus);
            dg_upgrade.Columns[2].HeaderText = "ADD";
            if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill== "speed up")
            {
                dg_upgrade.Rows.RemoveAt(1);
                dg_upgrade.Rows.RemoveAt(1);
            }
            else if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill == "mine_bomb")
            {
                dg_upgrade.Rows.RemoveAt(0);
                dg_upgrade.Rows.RemoveAt(1);
            }
            else if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill == "eagle eye")
            {
                dg_upgrade.Rows.RemoveAt(0);
                dg_upgrade.Rows.RemoveAt(0);
            }
        }

        private void shop_game_Load(object sender, EventArgs e)
        {
            this.back.BackgroundImage = new Bitmap("buttonbacktomainmenu.png");
            this.back.BackgroundImageLayout = ImageLayout.Stretch;
            this.back.FlatStyle = FlatStyle.Flat;
            this.back.BackColor = Color.Transparent;

            buatgridbomb();
            buatgridglove();
            //load 
            //player
            arr_save[0] = "saveplayer1.xml";
            arr_save[1] = "saveplayer2.xml";
            arr_save[2] = "saveplayer3.xml";
            arr_save[3] = "saveplayer4.xml";
            //mine
            arr_mine[0] = "saveplayer1mine.xml";
            arr_mine[1] = "saveplayer2mine.xml";
            arr_mine[2] = "saveplayer3mine.xml";
            arr_mine[3] = "saveplayer4mine.xml";
            //bomb
            arr_bomb[0] = "saveplayer1bomb.xml";
            arr_bomb[1] = "saveplayer2bomb.xml";
            arr_bomb[2] = "saveplayer3bomb.xml";
            arr_bomb[3] = "saveplayer4bomb.xml";
            //gambar player
            arr_gambar[0] = "hitam1.png";
            arr_gambar[1] = "hijau1.png";
            arr_gambar[2] = "kuning1.png";
            arr_gambar[3] = "putih1.png";
            int temp_halaman = ((Form1)MdiParent).save_or_load;
            if (temp_halaman==0)
            {
                panel1.BackgroundImage = new Bitmap(arr_gambar[((Form1)MdiParent).menu_select_player.chooseplayer]);
            }
            else if (temp_halaman == 1)
            {
                panel1.BackgroundImage = new Bitmap(arr_gambar[((Form1)MdiParent).menu_select_player_load.chooseplayer]);
                panel1.BackgroundImageLayout = ImageLayout.Stretch;
            }
            //isi player agar tidak null
            for (int i = 0; i < 4; i++)
            {
                arr_unit[i] = new unit(new Label(),i);
            }
            //load semua player
            for (int i = 0; i < 4; i++)
            {
                //load inventory
                XmlSerializer serializer = new XmlSerializer(typeof(inventory_unit));
                using (FileStream stream = File.OpenRead(arr_save[i]))
                {
                    arr_unit[i].bag = (inventory_unit)serializer.Deserialize(stream);
                }
                //load mine
                XmlSerializer serializer1 = new XmlSerializer(typeof(inventory_bomb));
                using (FileStream stream = File.OpenRead(arr_mine[i]))
                {
                    arr_unit[i].mine.isi_bomb = (inventory_bomb)serializer1.Deserialize(stream);
                }
                //load bomb
                XmlSerializer serializer2 = new XmlSerializer(typeof(inventory_bomb));
                using (FileStream stream = File.OpenRead(arr_bomb[i]))
                {
                    arr_unit[i].bomb.Clear();
                    for (int j = 0; j < arr_unit[i].bag.jumlah_bomb; j++)
                    {
                        arr_unit[i].bomb.Add(new splash_bomb(new Label()));
                        arr_unit[i].bomb[j].label_unit.BackColor = Color.Transparent;
                        arr_unit[i].bomb[j].label_unit.FlatStyle = FlatStyle.Flat;
                        arr_unit[i].bomb[j].label_unit.Location = new Point(arr_unit[i].label_unit.Location.X, arr_unit[i].label_unit.Location.Y - 20);
                        arr_unit[i].bomb[j].label_unit.Size = new Size(50, 50);
                    }
                    inventory_bomb temp = (inventory_bomb)serializer2.Deserialize(stream);
                    foreach (splash_bomb x in arr_unit[i].bomb)
                    {
                        x.isi_bomb.horizontal = temp.horizontal;
                        x.isi_bomb.vertical = temp.vertical;
                        x.label_unit.Image = new Bitmap("bomb1.png");
                    }
                }
            }
            buatgridupgrade();
            //load inventory ke shop
            refreshList();
            cekUang();
        }

        private void back_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                //save inventory
                int player = ((Form1)MdiParent).menu_select_player.chooseplayer;
                System.IO.File.WriteAllText(arr_save[i], string.Empty);
                Stream stream = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_save[i]);
                XmlSerializer xmlser = new XmlSerializer(typeof(inventory_unit));
                xmlser.Serialize(stream, arr_unit[i].bag);
                stream.Close();
                //save mine
                System.IO.File.WriteAllText(arr_mine[i], string.Empty);
                Stream stream1 = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_mine[i]);
                XmlSerializer xmlser1 = new XmlSerializer(typeof(inventory_bomb));
                xmlser1.Serialize(stream1, arr_unit[i].mine.isi_bomb);
                stream1.Close();
                //save bomb
                System.IO.File.WriteAllText(arr_bomb[i], string.Empty);
                Stream stream2 = File.OpenWrite(Environment.CurrentDirectory + "\\" + arr_bomb[i]);
                XmlSerializer xmlser2 = new XmlSerializer(typeof(inventory_bomb));
                xmlser2.Serialize(stream2, arr_unit[i].bomb[0].isi_bomb);
                stream2.Close();
            }
            ((Form1)MdiParent).change_to_menu_game();
        }
        private void dg_bomb_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && e.RowIndex == 0)
            {
                pbomb.BackgroundImage = new Bitmap("bomb+1.png");
                pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                pdeskripsi.BackgroundImage = new Bitmap("descbomup.png");
                pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
            }
            else if (e.ColumnIndex == 1 && e.RowIndex == 1)
            {
                pbomb.BackgroundImage = new Bitmap("horizon1.png");
                pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                pdeskripsi.BackgroundImage = new Bitmap("deschorizontalbom+1.png");
                pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
            }
            else if (e.ColumnIndex == 1 && e.RowIndex == 2)
            {
                pbomb.BackgroundImage = new Bitmap("vertical1.png");
                pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                pdeskripsi.BackgroundImage = new Bitmap("descverticalbom+1.png");
                pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
            }
            else if (e.ColumnIndex == 1 && e.RowIndex == 3)
            {
                pbomb.BackgroundImage = new Bitmap("plus1.png");
                pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                pdeskripsi.BackgroundImage = new Bitmap("descbomplus.png");
                pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
            }
            if (e.ColumnIndex == 0 && e.RowIndex == 0)
            {
                MessageBox.Show("Bomb Up +1");
                //ISI untuk  nambah bomb +1
                if (cekBisaUpgrade())
                {
                    cekUang();
                    if (uangPlayer - 150 >= 0)
                    {
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.jumlah_bomb += 1;
                        reduceUang(150);
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.listUpgrade[arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade] = "Bomb +1";
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade += 1;
                        refreshList();
                        MessageBox.Show("Item Bomb Up +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                    }
                    else { no_money(); }
                }
                else
                {
                    MessageBox.Show("Anda sudah mencapai batas upgrade!");
                }
            }
            else if (e.ColumnIndex == 0 && e.RowIndex == 1)
            {
                MessageBox.Show("horizontal bomb +1");
                //ISI untuk nambah horizon +1
                if (cekBisaUpgrade())
                {
                    if (uangPlayer - 200 >= 0)
                    {
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bomb[0].isi_bomb.horizontal += 1;
                        reduceUang(200);
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.listUpgrade[arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade] = "Bomb Horizontal +1";
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade += 1;
                        refreshList();
                        MessageBox.Show("Radius horizontal bomb +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                    }
                    else { no_money(); }
                }
                else
                {
                    MessageBox.Show("Anda sudah mencapai batas upgrade!");
                }
            }
            else if (e.ColumnIndex == 0 && e.RowIndex == 2)
            {
                MessageBox.Show("Vertikal bomb +1");
                //ISI untuk  nambah vertical +1
                if (cekBisaUpgrade())
                {
                    if (uangPlayer - 200 >= 0)
                    {
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bomb[0].isi_bomb.vertical += 1;
                        reduceUang(200);
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.listUpgrade[arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade] = "Bomb Vertikal +1";
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade += 1;
                        refreshList();
                        MessageBox.Show("Vertikal bomb +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                    }
                    else { no_money(); }
                }
                else
                {
                    MessageBox.Show("Anda sudah mencapai batas upgrade!");
                }
            }
            else if (e.ColumnIndex == 0 && e.RowIndex == 3)
            {
                MessageBox.Show("bomb plus +1");
                //ISI untuk bom plus +1
                if (cekBisaUpgrade())
                {
                    if (uangPlayer - 200 >= 0)
                    {
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bomb[0].isi_bomb.horizontal += 1;
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bomb[0].isi_bomb.vertical += 1;
                        reduceUang(200);
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.listUpgrade[arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade] = "Bomb Plus +1";
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade += 1;
                        refreshList();
                        MessageBox.Show("Radius bomb plus +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                    }
                    else { no_money(); }
                }
                else
                {
                    MessageBox.Show("Anda sudah mencapai batas upgrade!");
                }
            }
        }
        private void dg_glove_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dg_glove.CurrentRow.Index == 0)
            {
                pbomb.BackgroundImage = new Bitmap("glove1.png");
                pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                pdeskripsi.BackgroundImage = new Bitmap("descglove1.png");
                pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
            }
            else if (dg_glove.CurrentRow.Index == 1)
            {
                pbomb.BackgroundImage = new Bitmap("glove2.png");
                pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                pdeskripsi.BackgroundImage = new Bitmap("descglove2.png");
                pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
            }
        }

        private void dg_upgrade_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dg_upgrade.CurrentRow.Index == 0)
            {
                if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill=="speed up")
                {
                    pbomb.BackgroundImage = new Bitmap("speedup1.png");
                    pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                    pdeskripsi.BackgroundImage = new Bitmap("descspeedup.png");
                    pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
                }
                else if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill == "mine_bomb")
                {
                    pbomb.BackgroundImage = new Bitmap("minebomb.png");
                    pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                    pdeskripsi.BackgroundImage = new Bitmap("descminebomb.png");
                    pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
                }
                else if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill == "eagle eye")
                {
                    pbomb.BackgroundImage = new Bitmap("eagle.png");
                    pbomb.BackgroundImageLayout = ImageLayout.Stretch;
                    pdeskripsi.BackgroundImage = new Bitmap("desceagleeye.png");
                    pdeskripsi.BackgroundImageLayout = ImageLayout.Stretch;
                }
            }

            if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill<3)
            {
                if (e.ColumnIndex == 0 && e.RowIndex == 0)
                {
                    if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill == "speed up")
                    {
                        MessageBox.Show("Speed Up +1");
                        //ISI untuk  nambah speed up +1
                        if (cekBisaUpgrade())
                        {
                            if (uangPlayer - 150 >= 0)
                            {
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.Movement_Speed += 15;
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.mana_skill += 15;
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill += 1;
                                reduceUang(150);
                                refreshList();
                                MessageBox.Show("Item Speed Up +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                            }
                            else { no_money(); }
                        }
                        else
                        {
                            MessageBox.Show("Anda sudah mencapai batas upgrade!");
                        }
                    }
                    else if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill == "mine_bomb")
                    {
                        MessageBox.Show("mine bomb +1");
                        //ISI untuk  nambah mine bomb +1
                        if (cekBisaUpgrade())
                        {
                            if (uangPlayer - 200 >= 0)
                            {
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].mine.isi_bomb.horizontal += 1;
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].mine.isi_bomb.vertical += 1;
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.mana_skill += 15;
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill += 1;
                                reduceUang(200);
                                MessageBox.Show("mine bomb +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                                refreshList();
                            }
                            else { no_money(); }
                        }
                        else
                        {
                            MessageBox.Show("Anda sudah mencapai batas upgrade!");
                        }
                    }
                    else if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill == "eagle eye")
                    {
                        MessageBox.Show("eagle eye +1");
                        //ISI untuk eagle eye
                        if (cekBisaUpgrade())
                        {
                            if (uangPlayer - 200 >= 0)
                            {
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.timeEE += 1;
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.mana_skill += 15;
                                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill += 1;
                                reduceUang(200);
                                refreshList();
                                MessageBox.Show("Item eagle eye +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                            }
                            else { no_money(); }
                        }
                        else
                        {
                            MessageBox.Show("Anda sudah mencapai batas upgrade!");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("level skill sudah maximal!");
            }
        }

        private void dg_glove_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex == 0)
            {
                //MessageBox.Show("tambah");
                //ISI untuk glove 1
                if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop !="Glove Level 1")
                {
                    cekUang();
                    if (uangPlayer - 200 >= 0)
                    {
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.time_planting= 150;
                        reduceUang(200);
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop = "Glove Level 1";
                        refreshList();
                        MessageBox.Show("Item Glove Level 1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                    }
                    else { no_money(); }
                }
                else if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop == "Glove Level 1")
                {
                    MessageBox.Show("Anda sudah pernah membeli item ini!");
                }
            }
            else if (e.ColumnIndex == 0 && e.RowIndex == 1)
            {
                MessageBox.Show("tambah1");
                //ISI untuk glove 2
                if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop != "Glove Level 2")
                {
                    cekUang();
                    if (uangPlayer - 300 >= 0)
                    {
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.time_planting = 100;
                        reduceUang(300);
                        arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop = "Glove Level 2";
                        refreshList();
                        MessageBox.Show("Item Glove Level 2 telah dibeli! Sisa uang Anda: " + uangPlayer);
                    }
                    else { no_money(); }
                }
                else if(arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop == "Glove Level 2")
                {
                    MessageBox.Show("Anda sudah pernah membeli item ini!");
                }
            }
        }
        public void cekUang()
        {
            uangPlayer =arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.uang;
            labelUang.Text = uangPlayer.ToString();
        }
        void reduceUang(int duit) {
            arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.uang-=duit;
            cekUang();
        }
        void no_money() { MessageBox.Show("Maaf, uang Anda tidak cukup :("); }
        public void refreshList() {
            labelUang.Text = uangPlayer.ToString();
            label_skillplayer.Text = arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop;
            String upgrades = "";
            for (int i = 0; i < 3; i++)
            {
                upgrades += arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.listUpgrade[i]+ ",";
            }
            label_upgrade.Text = upgrades;
            label_bomplayer.Text = "BOMB\n"+"VERTICAL : "+arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bomb[0].isi_bomb.vertical+"\nHORIZONTAL : "+ arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bomb[0].isi_bomb.horizontal+"\nSKILL : "+ arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.skill+"\nLEVEL SKILL : " + arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill;
        }
        bool cekBisaBomb() {
            if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistBomb == 3) { return false; }
            else { return true; }
        }
        bool cekBisaUpgrade() {
            if (arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.ctrlistUpgrade == 3) { return false; }
            else { return true; }
        }

        private void pbomb_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cekBisaUpgrade())
            {
                if (uangPlayer - 20 >= 0)
                {
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].mine.isi_bomb.horizontal += 1;
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].mine.isi_bomb.vertical += 1;
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.mana_skill += 15;
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill += 1;
                    reduceUang(200);
                    MessageBox.Show("mine bomb +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                    refreshList();
                }
                else { no_money(); }
            }
            else
            {
                MessageBox.Show("Anda sudah mencapai batas upgrade!");
            }
            if (cekBisaUpgrade())
            {
                if (uangPlayer - 50 >= 0)
                {
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.Movement_Speed += 15;
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.mana_skill += 15;
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill += 1;
                    reduceUang(150);
                    refreshList();
                    MessageBox.Show("Item Speed Up +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                }
                else { no_money(); }
            }
            else
            {
                MessageBox.Show("Anda sudah mencapai batas upgrade!");
            }
            
            
            if (cekBisaUpgrade())
            {
                if (uangPlayer - 20 >= 0)
                {
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.timeEE += 1;
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.mana_skill += 15;
                    arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.level_skill += 1;
                    reduceUang(200);
                    refreshList();
                    MessageBox.Show("Item eagle eye +1 telah dibeli! Sisa uang Anda: " + uangPlayer);
                }
                else { no_money(); }
            }
            else
            {
                MessageBox.Show("Anda sudah mencapai batas upgrade!");
            }
            if (uangPlayer - 20 >= 0)
            {
                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.time_planting = 150;
                reduceUang(200);
                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop = "Glove Level 1";
                refreshList();
                MessageBox.Show("Item Glove Level 1 telah dibeli! Sisa uang Anda: " + uangPlayer);
            }
            else { no_money(); }
            if (uangPlayer - 30 >= 0)
            {
                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.time_planting = 100;
                reduceUang(300);
                arr_unit[((Form1)MdiParent).menu_select_player.chooseplayer].bag.gloveShop = "Glove Level 2";
                refreshList();
                MessageBox.Show("Item Glove Level 2 telah dibeli! Sisa uang Anda: " + uangPlayer);
            }
            else { no_money(); }
        }
    }
}
