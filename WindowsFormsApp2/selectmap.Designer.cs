﻿namespace WindowsFormsApp2
{
    partial class selectmap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.arrow_left = new System.Windows.Forms.Button();
            this.arrow_right = new System.Windows.Forms.Button();
            this.gambar_map = new System.Windows.Forms.Panel();
            this.btnchoose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // arrow_left
            // 
            this.arrow_left.Location = new System.Drawing.Point(240, 273);
            this.arrow_left.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.arrow_left.Name = "arrow_left";
            this.arrow_left.Size = new System.Drawing.Size(157, 94);
            this.arrow_left.TabIndex = 0;
            this.arrow_left.UseVisualStyleBackColor = true;
            this.arrow_left.Click += new System.EventHandler(this.arrow_left_Click);
            // 
            // arrow_right
            // 
            this.arrow_right.Location = new System.Drawing.Point(1333, 273);
            this.arrow_right.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.arrow_right.Name = "arrow_right";
            this.arrow_right.Size = new System.Drawing.Size(157, 94);
            this.arrow_right.TabIndex = 1;
            this.arrow_right.UseVisualStyleBackColor = true;
            this.arrow_right.Click += new System.EventHandler(this.arrow_right_Click);
            // 
            // gambar_map
            // 
            this.gambar_map.Location = new System.Drawing.Point(597, 66);
            this.gambar_map.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gambar_map.Name = "gambar_map";
            this.gambar_map.Size = new System.Drawing.Size(592, 451);
            this.gambar_map.TabIndex = 2;
            this.gambar_map.Paint += new System.Windows.Forms.PaintEventHandler(this.gambar_map_Paint);
            // 
            // btnchoose
            // 
            this.btnchoose.Location = new System.Drawing.Point(673, 609);
            this.btnchoose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnchoose.Name = "btnchoose";
            this.btnchoose.Size = new System.Drawing.Size(425, 94);
            this.btnchoose.TabIndex = 3;
            this.btnchoose.UseVisualStyleBackColor = true;
            this.btnchoose.Click += new System.EventHandler(this.btnchoose_Click);
            // 
            // selectmap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 750);
            this.Controls.Add(this.btnchoose);
            this.Controls.Add(this.gambar_map);
            this.Controls.Add(this.arrow_right);
            this.Controls.Add(this.arrow_left);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "selectmap";
            this.Text = "selectmap";
            this.Load += new System.EventHandler(this.selectmap_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button arrow_left;
        private System.Windows.Forms.Button arrow_right;
        private System.Windows.Forms.Panel gambar_map;
        private System.Windows.Forms.Button btnchoose;
    }
}